import os
import logging
import timber


log_source_id = os.getenv('timber_source_id')
log_apikey = os.getenv('timber_apikey')

if (log_source_id and log_apikey) is not None:
    handler = timber.TimberHandler(source_id=log_source_id,  api_key=log_apikey)
else:
    logging.getLogger(__name__).warning("Timber credentials not found, using default logging handler")
    handler = logging.lastResort
    handler.setLevel(logging.INFO)


def lazy_logger(module_name):
    # lazy loggers that will be created once needed
    def _inner():
        logger = logging.getLogger(module_name)
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)
        return logger
    return _inner
