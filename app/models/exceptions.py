from app.exceptions import ApplicationError


class ModelError(ApplicationError):
    def get_message(self):
        return "Error while doing things with model"
