from operator import mul
from functools import reduce

from app.exceptions import RequestValueError
from app.log import lazy_logger

logger = lazy_logger(__name__)


def hello():
    return "Hello, World!"


def product(number_str):
    digits = list(map(int, number_str))
    return reduce(mul, digits, 1)


def post_product(body):
    number_str = str(body['number'])
    logger().info("Get digits to product: %s", number_str)

    try:
        return {'tolmachev_best_result': product(number_str)}
    except ValueError as e:
        raise RequestValueError('number', 'unsigned int') from e
