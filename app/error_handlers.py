from connexion import Api
from connexion.problem import problem
from werkzeug.exceptions import HTTPException
from flask import request

from app.log import lazy_logger
from app.exceptions import ApplicationError

logger = lazy_logger('app')


def log_error(message):
    logger().error(
        message,
        exc_info=True, extra={'request': {
            'data': request.data or '<empty>'
        }})


def response(p):
    return Api.get_response(p)


def handle_application_error(error: ApplicationError):
    log_error("Application error")
    return response(problem(error.status_code, error.__name__, error.get_message()))


def handle_other_errors(error: Exception):
    if isinstance(error, HTTPException):
        log_error("Unhandled HTTP exception")
        return response(problem(error.code, error.name, error.description))
    else:
        log_error("Unexpected server error")
        return response(problem(500, "Unexpected error", "Unexpected server error encountered"))
