from app.log import lazy_logger
from app.helpers import get_random_string
from app.models import main as models_main
from app.models import exceptions as models_exc

LOG_MSG_BASE = 'internal_id: %s,  step: %s'
LOG_MSG_PARAMS = 'internal_id: %s,  step: %s,  message_id: %s'

logger = lazy_logger(__name__)


def get_message(body):
    internal_id = get_random_string(20)

    logger().info(
        LOG_MSG_PARAMS, internal_id, 'Got /get_message with params', body['message_id'],
        extra={'params': body})

    response = {'message_id': body['message_id'], 'dialog_id': body['dialog_id'],
                'participants_id': body['participants_id'], 'user_id': body['user_id'], 'models': []}

    try:
        response['models'] = models_main.main(
            json_params=body, model_to='message_id')

        logger().info(
            LOG_MSG_PARAMS, internal_id, 'Model done', body['message_id'],
            extra={'params': body})
    except models_exc.ModelError as e:
        logger().error(
            LOG_MSG_PARAMS, internal_id, 'Error when running model', body['message_id'],
            extra={'params': body})
        raise e

    return response
