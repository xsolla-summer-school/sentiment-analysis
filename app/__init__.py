import connexion
from connexion.problem import problem

from app import error_handlers
from app.exceptions import ApplicationError


application = connexion.App(__name__, specification_dir='openapi/')
application.add_api('api.yaml')
# noinspection PyTypeChecker
application.add_error_handler(ApplicationError, error_handlers.handle_application_error)
# noinspection PyTypeChecker
application.add_error_handler(Exception, error_handlers.handle_other_errors)
app = application.app
