class ApplicationError(Exception):
    status_code = 500

    def get_message(self):
        return "Application error"


class UserFacingError(ApplicationError):
    status_code = 400

    def get_message(self):
        return "Bad user request error"


class MalformedRequestError(UserFacingError):
    pass


class ExpectedJsonError(MalformedRequestError):
    def get_message(self):
        return "Error parsing json request data"


class RequestKeyError(UserFacingError):
    def get_message(self):
        return f"Key '{self.args[0]}' not found in request body"


class RequestValueError(UserFacingError):
    def get_message(self):
        return f"Error parsing key '{self.args[0]}': expected type {self.args[1]}"
